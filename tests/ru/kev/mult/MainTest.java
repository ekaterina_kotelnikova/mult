package ru.kev.mult;

import org.junit.Test;

import java.util.Random;

import static org.junit.Assert.*;

/**
 * Unit тесты для проверки работоспособности методов класса Main
 *
 * @author Kotelnikova E.V. group 15it18
 */
public class MainTest {
    @Test
    public void testModule() throws Exception {
        for (int i = 0; i < 1_000; i++) {
            Random rand = new Random();
            int a = rand.nextInt(200) - 100;
            assertEquals(Math.abs(a), Main.module(a));
        }
    }

    @Test
    public void testIsNegative() throws Exception {
        for (int i = 0; i < 1_000_000; i++) {
            Random rand = new Random();
            int a = rand.nextInt(200) - 100;
            int b = rand.nextInt(200) - 100;

            boolean isNeg = true;
            if (a >= 0 & b >= 0 || a <= 0 & b <= 0) {
                isNeg = false;
            }
            assertEquals(isNeg, Main.isNegative(a, b));
        }
    }

    @Test
    public void testMultiplication() throws Exception {
        for (int i = 0; i < 1_000_000; i++) {
            Random rand = new Random();
            int a = rand.nextInt(200) - 100;
            int b = rand.nextInt(200) - 100;
            assertEquals(a * b, Main.multiplication(a, b));
        }
    }


}