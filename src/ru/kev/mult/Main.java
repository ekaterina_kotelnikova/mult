package ru.kev.mult;

import java.util.Scanner;

/**
 * Класс для умножения двух чисел без использования операции умножения
 *
 * @author Kotelnikova E. group 15it18
 */
public class Main {

    static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.println("Введите первое число:");
        int firstNumber = scanner.nextInt();
        System.out.println("Введите второе число:");
        int secondNumber = scanner.nextInt();


        int result = multiplication(firstNumber, secondNumber);

        System.out.println("Результат: " + result);
    }

    /**
     * Вовращает результат умножения без использования операции умножения
     *
     * @param firstNumber  первое число
     * @param secondNumber второе число
     * @return firstNumber*secondNumber
     */

    public static int multiplication(int firstNumber, int secondNumber) {
        if (firstNumber == 0 || secondNumber == 0) {
            return 0;
        }

        boolean isNeg = isNegative(firstNumber,secondNumber);

        firstNumber = module(firstNumber);
        secondNumber = module(secondNumber);

        if (firstNumber > secondNumber) {
            int tmp = secondNumber;
            secondNumber = firstNumber;
            firstNumber = tmp;
        }

        int result = 0;
        while (firstNumber != 0) {
            result += secondNumber;
            firstNumber--;
        }

        return isNeg ? -result : result;
    }

    /**
     * Возвращает модуль числа
     * @param number число
     * @return |number|
     */

    protected static int module(int number) {
        return (number < 0) ? -number : number;
    }

    /**
     * Метод для определения знака.
     * @param firstNumber первый множитель
     * @param secondNumber второй множитель
     * @return true, если знак будет отрицательным, иначе false
     */

    protected static boolean isNegative(int firstNumber, int secondNumber) {
        return (firstNumber < 0 && secondNumber > 0) || (firstNumber > 0 && secondNumber < 0);
    }
}